#include<iostream>
#include <fstream>
#include <typeinfo>
#include "liste.h"
#include <stdlib.h>
#include <time.h>

using namespace std;


int main(int argc,char** argv)
{
    liste l;
	/* Le programme principal a un argument d'entrée	*/
	/* Le nom du fichier de test			*/
	if(argc < 2)
		cout << "Nombre d'arguments insuffisants" << endl;
	else{
		ifstream file(argv[1]);
		int n; // nombre d'entiers à lire
		int j;
		if(! file)
			cerr << "Erreur ouverture fichier" << argv[1] << endl;
		else{			
			file >> n;
		
			for(int i = 0; i < n ; i++){
				file >> j;
				maillon *x=new maillon(j);
				l.insertionEnTete(x);
			}
		}

		l.affichage(); // Affichage de la liste avant inversion
		l.inversion();
		l.affichage(); // Affichage de la liste après inversion

	}
	return 0;
}