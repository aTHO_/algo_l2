/****************************************/
/* Auteur : S. Gueye			*/
/* TP listes chainées, L2 Avignon	*/
/* Date dernière maj : 26/08/2019	*/
/****************************************/

/****************************************/
/* Objectif : Classe représentant un maillon
/****************************************/
class maillon
{
	public :
	int val;
	maillon * succ;
	maillon(int k);
	~maillon();
	void affichage();
};

/****************************************/
/* Objectif : Classe représentant une liste
/****************************************/
class liste
{
	friend class evaluate;
	maillon * tete;
	public :
	liste();
	~liste();
	bool vide();
	void affichage();
	void insertionEnTete(maillon* x);
	void inversion();
};