#include <iostream>
#include <cstdlib>
#include "liste.h"

using namespace std;

//#define DEBUG

/****************************************/
/* Objectif : Constructeur d'un maillon contenant un entier.
/* Entrées : entier x
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
maillon::maillon(int x)
{
	val = x;
	succ = nullptr;
}

/****************************************/
/* Objectif : Destructeur d'un maillon et de ses successeurs
/* Entrées : 
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
maillon::~maillon()
{
	delete(succ);
}

/****************************************/
/* Objectif : Affichage du contenu d'un maillon.
/* Entrées : 
/* Sorties  :
/* Complexité : 0(n) où n est la taille de liste. 
de maillons dont le premier élément est l'objet appelant.
/****************************************/
void maillon::affichage()
{
    cout << val ;
	if(succ){
		cout << " -> " ;
		succ->affichage();
	}
	else
		cout << endl;
}

/****************************************/
/* Objectif : Constructeur d'une liste doublement
chaînées d'entiers.
/* Entrées : 
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
liste::liste()
{
	tete = nullptr;
}


/****************************************/
/* Objectif : Destructeur d'une liste
/* Entrées : 
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
liste::~liste()
{
	if(!this -> vide())
	{
		delete(tete) ;
	}
}

/****************************************/
/* Objectif : Test de vacuité
/* Entrées : 
/* Sorties  : booléan : vrai si la liste est vide 
et faux sinon.
/* Complexité : 0(1)
/****************************************/
bool liste::vide()
{
	if(this -> tete == nullptr)
        {
            return(true);
        }
    else
        {
            return(false);
        }
}

/****************************************/
/* Objectif : Insertion en tête d'un maillon 
x supposé non nul.
/* Entrées : Pointeur sur le maillon à insérer.
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
void liste::insertionEnTete(maillon *x)
{
	if(!vide())
        {
            x -> succ = this -> tete ;
            this -> tete = x;
        }
    else
        {
            this -> tete = x ;
			x -> succ = nullptr;
        }
}


/****************************************/
/* Objectif : Insertion en tête d'un maillon 
/* x supposé non nul
/* Entrées : pointeur sur le maillon à insérer
/* Sorties  :
/* Complexité : 0(n) où n est le nombre de maillons
de la liste.
/****************************************/
void liste::affichage()
{
    	if(!vide())
    		tete->affichage();

}



void liste::inversion()
	{
		#ifdef DEBUG
			cout << "Enter in inversion() :" << endl;
		#endif
		if(!vide())
			{
				bool first = true;
				#ifdef DEBUG
					cout << "List not empty" << endl;
				#endif
				/*-- If 1 objet --*/
				if(this -> tete -> succ == nullptr)
					{
						#ifdef DEBUG
							cout << "Only one element in this L
							ist" << endl;
						#endif
						return;
					}
				else
				{
					maillon * TempGlobal = this -> tete;
					while(true)
						{
							if(tete -> succ != nullptr)
								{
									#ifdef DEBUG
										cout << "while  rep" << endl;
									#endif
									maillon * TempLocal = this -> tete;
									this -> tete = this -> tete -> succ;
									TempLocal -> succ = TempGlobal;
									TempGlobal = TempLocal;
									#ifdef DEBUG
										cout << "tete -> val = " << this -> tete -> val << endl;
										cout << "tete -> succ = " << this -> tete -> succ << endl;
										cout << "TempGlobal = " << TempGlobal << " | Value = " << TempGlobal-> val << endl;
									#endif
								}
							else
								{
									break;
								}
							if(first)
							{
								#ifdef DEBUG
									cout << "First" << endl;
								#endif
								TempGlobal -> succ = nullptr;
								first = false;
							}
						}
					#ifdef DEBUG
						cout << "Fin while : " << endl << "TempGlobal = " << TempGlobal << " | Value = " << TempGlobal-> val << endl;
						cout << "tete = " << this -> tete << endl;
						cout << "tete -> succ = " << this -> tete -> succ << endl; 
					#endif
					this -> tete -> succ = TempGlobal;
					return;
				}
			}
	} 