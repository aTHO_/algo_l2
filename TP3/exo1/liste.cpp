#include <iostream>
#include <cstdlib>
#include "liste.h"

using namespace std;


/****************************************/
/* Objectif : Constructeur d'un maillon contenant un entier.
/* Entrées : entier x
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
maillon::maillon(int x)
{
    this->val = x;
    this->succ = nullptr;
    this->pred = nullptr;
}


/****************************************/
/* Objectif : Destructeur d'un maillon et de ses successeurs
/* Entrées : 
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
maillon::~maillon()
{
    delete(succ);
}

/****************************************/
/* Objectif : Affichage (récursif) du contenu d'un maillon et de ses successeurs/
/* (Méthode donnée pour éviter des erreurs de format d'affichage)
/* Entrées : 
/* Sorties  :
/* Complexité : 0(n)
/****************************************/
void maillon::affichage()
{
    cout << val;
    if(succ){
        cout << " -> " ;
        succ->affichage();
	}
	else
		cout << endl;

}

/****************************************/
/* Objectif : Constructeur d'une liste doublement
chaînées d'entiers.
/* Entrées : 
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
liste::liste()
{
    this->tete = nullptr;
    this->queue = nullptr;
}


/****************************************/
/* Objectif : Destructeur d'une liste
/* Entrées : 
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
liste::~liste()
{
    this->tete->~maillon();
}

/****************************************/
/* Objectif : Test de vacuité
/* Entrées : 
/* Sorties  : booléan : vrai si la liste est vide 
et faux sinon.
/* Complexité : 0(1)
/****************************************/
bool liste::vide()
{
    if(this->tete == nullptr) //  && this->queue == nullptr inutile
        {
            return true;
        }
    return false;
}

/****************************************/
/* Objectif : Insertion en tête d'un maillon 
x supposé non nul.
/* Entrées : Pointeur sur le maillon à insérer.
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
void liste::insertionEnTete(maillon *x)
{
    if(this->vide())
        {
            this->tete = x;
            this->queue = x;
        } 
    else 
        {
            this->tete->pred = x;
            x->succ = this->tete;
            this->tete = x;
            x->pred = nullptr;
        }
}

/****************************************/
/* Objectif : Insertion en queue d'un maillon 
x supposé non nul
/* Entrées : Pointeur sur le maillon à insérer.
/* Sorties  :
/* Complexité : 0(1)
/****************************************/
void liste::insertionEnQueue(maillon *x)
{
    if(!vide())
        {
            x -> succ = nullptr;
            this -> queue -> succ = x;
            x -> pred = this -> queue ;
            this -> queue = x;
        }
    else
        {
            this -> tete = x ;
            this -> queue = x ;
        }

}

/****************************************/
/* Objectif : Affichage du contenu de la liste
/* Entrées : 
/* Sorties  :
/* Complexité : 0(n)
/****************************************/
void liste::affichage()
{
    if(!vide())
    	tete->affichage();

}

/****************************************/
/* Objectif : Recherche du premier maillon 
contenant la valeur x.
/* Entrées : Entier recherché.
/* Sorties  : adresse du maillon contenant x.
/* Complexité : 0(n) où n est le nombre de maillons
de la liste.
/****************************************/
maillon *liste::rechercher(int x)
{
    maillon * current = this->tete;
    while(current != this->queue )
        {
            if(current->val == x)
                {
                    return current;;
                }
            current = current->succ;
        }
    return nullptr;
}

/****************************************/
/* Objectif : Suppression d'un maillon x 
supposé existant dans la liste.
/* Entrées : Pointeur sur le maillon à supprimer.
/* Sorties  : adresse du maillon contenant x.
/* Complexité : 0(1)
/****************************************/
void liste::suppression(maillon *x)
{   
    if ( x -> pred != nullptr)
        {
            x -> pred -> succ= x -> succ;
        }
    else
        {
            this -> tete = x -> succ;
        }
    if ( x -> succ != nullptr)
        {
            x -> succ -> pred = x -> pred;
        }
    else
        {
            queue = x -> pred;
        }
}