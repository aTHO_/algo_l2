#include<iostream>
#include <fstream>
#include "liste.h"

using namespace std;


int main(int argc,char** argv)
{
	/* Le programme principa a un argument d'entrée	*/
	/* Le nom du fichier de test			*/
	if(argc < 2)
		cout << "Nombre d'arguments insuffisants" << endl;
	else{
		ifstream file(argv[1]);
		int i,j,n,val1,val2;
		liste l;

		if(! file)
			cerr << "Erreur ouverture fichier" << argv[1] << endl;
		else{			
			file >> n;
			maillon * x;

			for(i = 1; i <= n ; i++){
				file >> j;
				maillon *x=new maillon(j);
				l.insertionEnTete(x);
			}
			file >> val1;
			file >> val2;
 			file.close();
		}

		l.affichage();


		if(l.rechercher(val1))
			cout << val1 << " present" << endl;	
		else
			cout << val1 << " absent" << endl;	

		maillon* x = l.rechercher(val2);
		if(x)
			cout << val2 << " present" << endl;	
		else
			cout << val2 << " absent" << endl;	

		l.suppression(x);
		l.affichage();

	}
	return 0;
}