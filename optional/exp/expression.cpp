/****************************************/
/* Auteur : S. Gueye			*/
/* Date dernière maj : 19/09/2019	*/
/****************************************/

#include<iostream>
#include <fstream>
#include <stack>

using namespace std;

#include "expression.h"
/****************************************/
/* Objectif : Vérifier qu'une expression lue dans un fichier de nom
/* "filename" est équilibrée ou non
/* Entrées : 
/* Sorties  : vrai si elle est équilibré, faux sinon.
/* Complexité : 0(n) où n est le nombre de caractères de l'expression
/****************************************/

bool expression::equilibre(char* filename)
{
    
    /*
    Indication sur la lecture de fichier caractère par caractère
    ***********************************************************
    L'ouverture d'un fichier de nom "filename" en lecture se fait comme suit : 
    file.open(filename, std::ifstream::in);
    
    Elle place un "curseur" en début du fichier. On peut alors le lire caracère par caractère.
    
    Si "c" est de type char : char c;
    alors file >> c envoie dans "c" le caractère où se troue le curseur puis celui-ci passe au caractère suivant.
    
    La lecture de tous les caractères se fait comme suit :
    
    while(!file.eof())
        file >> c;
        
    Ces instructions ont la signification suivante. Tant qu'on est pas la fin fichier, lire le caractère courant est le mettre dans c.
    eof() est une méthode de la classe ifstream indiquant que la fin du fichier.
    
    La fermeture après utilisation du fichier se fait par l'appel :
    file.close()
    */
    file.open(filename, std::ifstream::in);
    char ch;
    bool out = true;    
    char x;
    stack<char> s;
    while(!file.eof())
    {
        
        file >> c;
        ch += c;
                if (c == '(' || c == '['
            || c == '{')
        {
            // Push the element in the stack
            s.push(c);
            continue;
        }
 
        // IF current current character is not opening
        // bracket, then it must be closing. So stack
        // cannot be empty at this point.
        if (s.empty())
            return false;
 
        switch (c) {
        case ')':
             
            // Store the top element in a
            x = s.top();
            s.pop();
            if (x == '{' || x == '[')
                return false;
            break;
 
        case '}':
 
            // Store the top element in b
            x = s.top();
            s.pop();
            if (x == '(' || x == '[')
                return false;
            break;
 
        case ']':
 
            // Store the top element in c
            x = s.top();
            s.pop();
            if (x == '(' || x == '{')
                return false;
            break;
    }
 
    // Check Empty Stack
    if(s.empty()){
        return true;
    }
    }
    cout << ch << "\"" << endl;

    file.close();
    return(out);
}