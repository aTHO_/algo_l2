#include<iostream>

using namespace std;

int compare(char * mot1, char * mot2, int n){
    int out;
    for(int i =0; i<n; i++){
        if(mot1[i] == mot2[i]){
            out = 0;
        }else if(mot1[i] > mot2[i]){
            return -1;
        }else{
            return 1;
        }
    }
    return out;
}

int main(){
    
    int n;
    cout << "Entré n : ";
    cin >> n;
    char a[n], b[n];
    cout << "Entré a : ";
    cin >> a;
    cout << "Entré b : ";
    cin >> b;
    cout << compare(a, b, n) << endl;
    return 0;
}