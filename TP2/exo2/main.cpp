#include<iostream>

using namespace std;

#define DEBUG

int* fusion(int * T1, int n1, int * T2, int n2){
    #ifdef DEBUG
        cout << "Inside fusion" << endl;
    #endif
    int size = n1 + n2 + 1;
    static int out[101];
    int indT1 = 0 ;
    int indT2 = 0;
    int i = 0;
    #ifdef DEBUG
        cout << "variable :" << endl;
        cout << "\tsize = " << size << endl;
        cout << "\ti = " << i << endl;
        cout << "\tindT2 = " << indT2 << endl;
        cout << "\tindT1 = " << indT1 << endl;
    #endif
    while(n1 > indT1 || n2 > indT2){
    #ifdef DEBUG
        cout << "in While" << endl;
    #endif
        if(T1[indT1] <= T2[indT2]){
            out[i] = T1[indT1];
            indT1++;
        } else {
            out[i] = T2[indT2];
            indT2++;
        }
        i++;
    }
    if(n1 > indT1){
        for(int j = i; j < size; j++){
            out[j] = T2[indT2];
        }
    } else {
        for(int j = i; j < size; j++){
            out[j] = T1[indT1];
        }
    }
    #ifdef DEBUG
        cout << "variable :" << endl;
        cout << "\tsize = " << size << endl;
        cout << "\ti = " << i << endl;
        cout << "\tindT2 = " << indT2 << endl;
        cout << "\tindT1 = " << indT1 << endl;
    #endif
    return out;
}

void print(int * T, int n){
    cout << "[" ;
    for(int i = 0; i < n; i++){
        if(i != n -1){
            cout << T[i] << ", ";
        } else {
            cout << T[i] ;
        }
    }
    cout << "]" << endl;
}

int main(){
    int T1[10] = {1, 2, 4, 6, 7 ,8 ,9 ,10, 11, 30};
    int T2[10] = {3, 4, 4, 5, 10 ,12 ,12 ,13, 14, 14};
    int* T3 = fusion(T2, 10, T1, 10);
    #ifdef DEBUG
        cout << "T1 and T2 done"<< endl;
    #endif
    cout << T3[0] << endl;
    print(T3, 20);
    return 0;
}