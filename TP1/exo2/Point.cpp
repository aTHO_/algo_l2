#include<iostream>
#include "Point.h"
#include<math.h>

using namespace std;


// Construct without ARG
Point::Point(){ x=0; y=0; }
	
// Setter for this->x and this->y
void Point::coord( double inputX, double inputY ){ x = inputX; y = inputY; }


// Calcul distance btw this a inputPoint
double Point::distance(Point inputPoint){
	return (sqrt( pow(inputPoint.x - this->x,2) + pow( inputPoint.y - this->y, 2)) );
}

void  Point::toPrint(){
	cout << "(" << this->x << "," << this->y << ")" << endl;
}
