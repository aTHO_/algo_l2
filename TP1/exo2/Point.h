#include<iostream>

using namespace std;

class Point {
	private :
		double x;
		double y;

	public :
		Point();
		void coord(double ,double);
		double distance(Point);
		void toPrint();
		      
};
