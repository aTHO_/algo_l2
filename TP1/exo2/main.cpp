#include<iostream>
#include "Point.h"

using namespace std;

int main(){
        int N;
        cout << "Enter N :";
        cin >> N;
        Point T[N];
        for(int i = 0; i < N; i++){
                T[i].coord((rand() % 1000)/1000, (rand() % 1000)/1000);
        }
        double d;
        cout << "Enter d :";
        cin >> d;
        for(int i = 0; i < N-1; i++){
                for(int j = i; j < N; j++){
                        if(T[i].distance(T[j]) <= d){
                                cout << "(" << i << "," << j << ")" << endl;
                        }
                }
        }
        return 0;
}
