
#include<iostream>
#include<string>

using namespace std;

int main(){
	int intDay;
	string day[7] = {"Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi","Dimanche"};
	cout << "Entrer une valeur entre 1 et 7 : ";
	cin >> intDay;
	if( intDay > 0 && intDay < 7){
		cout << day[intDay - 1] << endl;
	}
	return 0;
}