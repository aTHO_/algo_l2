/***************************************************************************************/
/*----- Auteur :        Aubertin Emmanuel               |  For: "Numbers round"     ****/
/*----- Description :   Simple solver for "Numbers round"                           ****/
/*----- Contact :       https://athomisos.fr                                        ****/
/***************************************************************************************/

#include <iostream>
#include <string.h>
#include <list>
#include <stdlib.h>
#include <fstream>
#include <functional>
#include <algorithm>
#include <iterator>

using namespace std;


string PROGNAME="Numbers round solver";
string FILE_NAME= __FILE__;
string RELEASE="Revision 1.0 | Last update 29 december 2021";
string AUTHOR="\033[1mAubertin Emmanuel\033[0m";
string COPYRIGHT="(c) 2021 "+ AUTHOR + " from https://athomisos.fr";
bool DEBUG = false;

// Prototypes
void print_list(list<int> L);
int lecture(list<int>* L, string filename);
list<int> calcul(list<int> L, list<int>::iterator i, list<int>::iterator j,  char op);
function<int(int,int)> parse_op(char op);
bool enumeration(list<int> L,int cible,int& approx,string& sol,string& bestsol);

void print_release() {
    cout << RELEASE << COPYRIGHT << endl;
}

void print_usage() {
        cout << endl 
        << PROGNAME << " by " << AUTHOR <<endl 
        << "\033[1mUsage: \033[0m"<< FILE_NAME <<" | [-h | --help] | [-v | --version] | [-d | --debug] | filename" << endl
        << "          -h        help" << endl
        << "          -v        Version" << endl
        << "          -d        Debug" << endl
        << "          filename  'number.txt' by default" << endl;
}

void print_help() {
        print_release();
        cout << endl;
        print_usage();
        cout << endl << endl;
        exit(0);
}

void failure(string message){
    cerr << "❌ \033[1;31m Error :\033[0m " << message << " ❌" << endl;
    exit(-1);
}

int main(int argc,char** argv){
    string filename = "number.txt"; // default filename
    if(argc < 1)
		failure("One argument required. \n\t-h for help");
    for(int i = 1; i < argc; i++){
        if (!strcmp(argv[i] , "-h")){
            print_usage();
            exit(0);
        } else if (!strcmp(argv[i] , "-v")){
            print_release();
            exit(0);
         } else if (!strcmp(argv[i] , "-d")){
            cout << "✌  |Welcome in \033[1mDEBUG\033[0m mode| ✌ " << endl;
            DEBUG = true;
        } else {
            if(DEBUG){
                cout << "🗒  New Filename : \"" <<  argv[i] << "\" 🗒" << endl;
            }
            filename = argv[i];
        }
    }


    list<int> myList;
    int result = lecture(&myList, filename);

    cout << endl << "We need to find this number \"" << result << "\" with this list : ";
    print_list(myList);

    /*list<int> tempList;
    std::list<int>::iterator it1=myList.begin() ;
     std::list<int>::iterator it2=++myList.begin()++;
    tempList = calcul(myList, it1, it2, '+');
    print_list(tempList);*/
    int approx = 0; 
    string sol, bestsol;
    enumeration(myList, result, approx, sol, bestsol);

    cout << "Approx: " << approx << endl;

    cout << endl << "✅ DONE ✅" << endl;
    cout <<"👋 Good bye! 👋" << endl;
    return 0;
}

// CODE POUR LE TP
/*
 * Just print the list like : | E1 | E2 | ... | En |
*/
void print_list(list<int> L){
    cout << "| ";
    for (list<int>::iterator element = L.begin(); element != L.end(); element++){
            cout << *element << " | ";
    }
    cout << endl;
}

//QUESTION N°1 :
/*
 * Read the file to put the data in L and return the result to reach.
*/
int lecture(list<int>* L, string filename){
    if(DEBUG){
        cout << endl << "\033[1mlecture(L, " << filename << ") \033[0m{" << endl;
    }
    ifstream file(filename);
    if(!file){
        failure("Unable to open the file ->\033[1m "+ filename +"\033[0m" );
    } else {
        if(DEBUG){
            cout << "\tReading file..." << endl;
        }
        int result;
        int tempLocal;
        if(DEBUG){
            cout << "\tStore result" << endl;
        }
        file >> result;
        if(DEBUG){
            cout << "\tStarting to build the List" << endl;
        }
        for(int i = 1; i < file.tellg()+1; i++) {
            file >> tempLocal;
            L->emplace_back(tempLocal);
            if(DEBUG){
                cout << "\tAdd " << tempLocal << " to the List" << endl;
            }
        }
        if(DEBUG){
            cout << "\tThe List is now ready, return result (" << result << ")" << endl << "}" << endl;
        }
        return result;
    }
}

//QUESTION N°2
/*
 * Returns a function<int(int,int)> that performs the operation of "operand".
*/
function<int(int,int)> parse_op(char operand){
    if(operand == '+')
        return [&](int x, int y){ return x + y;};
    if(operand == '-')
        return [&](int x, int y){ return x - y;};
    if(operand == '*')
        return [&](int x, int y){ return x * y;};
    if(operand == '/')
        return [&](int x, int y){ if(y!=0) return x / y; else return -1;}; //Need to fix floating point
}


/*
 * Returns a list<int> with the result of the operation "op" of i and j.
*/
list<int> calcul(list<int> L, list<int>::iterator i, list<int>::iterator j,  char op){
    if(DEBUG){
        cout << "In calcul() :" << endl 
        << "\tthe list is :"; print_list(L); cout
        << "\tThe operand is : \"" << op  << "\"" << endl
        << "\ti : \"" << *i << "\"" << endl 
        << "\tj : \"" << *j << "\"" <<endl;
    }
    if(i == j)
        failure("In calcul() i and j must be diference");
    if(!(std::find(L.begin(), L.end(), *i) != L.end()) || !(std::find(L.begin(), L.end(), *j) != L.end()))
        failure("In calcul() i and j must be in the list");

    int result = parse_op(op)(*i,*j);
    if(result > 0){
        i= L.erase(i); // remove i from the list
        j= L.erase(j); // remove j from the list
        L.push_front(result); // Add result at the first position
        return L;
    }
}

//QUESTION N°3
/*
 * Returns true if "it" is in the list.
*/
bool is_in_list(list<int> L, list<int>::iterator it){
    return std::find(L.begin(), L.end(), *it) != L.end();
}

/*
 * Brute force the Numbers round problem
*/
bool enumeration(list<int> L,int cible,int& approx,string& sol,string& bestsol){
    char operand[4] {'+', '*', '/', '-'};
    char best_operand;
    list<int>::iterator it1=L.begin();
    list<int>::iterator it2=++L.begin()++;
    int val_it1 = *it1;
    int val_it2 = *it2;
    if(is_in_list(L, it1) && is_in_list(L, it2)){
        if(DEBUG){
            cout << *it1 << " and " << *it2 << " are in the list" << endl;
        }
        for(int i = 0; i < 4; i++){ // for all operand
            if(DEBUG){
                cout << endl << endl << "New iterator i = " << i <<endl;
                cout << "reset temp variable"<< endl;
            }
            list<int> tempLocal = L;        // templist for keep L safe
            it1=tempLocal.begin();          // Reset & Update it1
            it2=++tempLocal.begin()++;      // Reset & Update it2
            tempLocal = calcul(tempLocal, it1, it2, operand[i]);
            sol = '| ' + val_it1 + operand[i] + val_it2; // store curent op in sol
            it1 = tempLocal.begin(); // Update of the first element
            if(DEBUG){
                cout << val_it1 << operand[i] << val_it2 << " = " << *it1 << " | Approx = " << approx << endl;
                cout << "(cible - approx) = " << cible - approx << " | (cible - *it1) = " << cible - *it1 << endl;
            }
            if(!((cible - approx) < (cible - *it1))){
                if(DEBUG){
                    cout << "Update of the best :" << val_it1 << operand[i] << val_it2 << " = " << *it1 << endl;
                }
                best_operand = operand[i]; // store the best
                approx = *it1; //update the best approx
            }
            if(DEBUG){
                cout << "not better";
            }
            if(cible == approx){
                return true;
            }
            if(DEBUG){
                cout << " and not perfect" << endl;
                cout << " i = " << i << endl;
            }
        }
        bestsol += sol; // update Bestsol with the last operation
        if(DEBUG){
            cout << "Out of for" << endl;
        }
        it1=L.begin();
        it2=++L.begin()++;
        if(DEBUG){
            cout << "old list: " ; print_list(L);
        }
        L = calcul(L, it1, it2, best_operand); //Update of the list
        if(DEBUG){
            cout << "New list: " ; print_list(L);
            cout << "-------------------------"<<endl;
        }
        if(L.size() > 1)
            return(enumeration(L, cible, approx, sol, bestsol));
    }
    if(DEBUG){
        cout << *it1 << " and " << *it2 << " aren't in the list" << endl;
        //bool testit1 = ;
        cout << "it1 ("<< *it2 <<") test : " << (find(L.begin(), L.end(), *it1) != L.end()) << endl;
        cout << "it2 ("<< *it2 <<") test : " << (find(L.begin(), L.end(), *it2) != L.end()) << endl;
    }
    if(!((approx - cible) < (*it1 - cible))){
        bestsol = sol;
        approx = *it1;
    }
    return true;
}


/*
J'ai stop sur une erreur un peu WTF.

New iterator i = 0
reset temp variable
In calcul() :
        the list is :| 2 | 4 | 5 | 8 | 8 | 75 | 
        The operand is : "+"
        i : "2"
        j : "4"
2+4 = 6 | Approx = 0
(cible - approx) = 448 | (cible - *it1) = 442
Update of the best :2+4 = 6
not better and not perfect
 i = 0


New iterator i = 1
reset temp variable
In calcul() :
        the list is :| 2 | 4 | 5 | 8 | 8 | 75 | 
        The operand is : "-"
        i : "2"
        j : "4"
2-4 = 4 | Approx = 6
(cible - approx) = 442 | (cible - *it1) = 444
not better and not perfect
 i = 1
make: *** [makefile:13: test] Segmentation fault (core dumped)
*/