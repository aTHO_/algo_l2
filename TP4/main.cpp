#include <iostream>
#include <stdlib.h>     /* srand, rand */
#include <time.h>  
#include "AB.h"

using namespace std;


int main(int argc, char** argv)
{
	int T[9] = {15, 32, 36, 10, 7, 3, 9, 8, 13};

	AB ab(new noeud(T[0],new noeud(T[1],new noeud(T[2],new noeud(T[5]), 
				new noeud(T[6],new noeud(T[7]), new noeud(T[8]))), 
				new noeud(T[3])), 
				new noeud(T[4])));

	cout << endl << "--------------------------------------------" << endl;
	cout << "Hauteur = \t" << ab.Hauteur(ab.root());
	cout << endl << "--------------------------------------------" << endl;
	cout << "Les noeuds du niveau 0 de l'arbre sont : ";
	ab.Niveau(ab.root(),0,0);	
	cout << endl << "--------------------------------------------" << endl;
	cout << "Les noeuds du niveau 1 de l'arbre sont : ";
	ab.Niveau(ab.root(),1,0);	
	cout << endl << "--------------------------------------------" << endl;
	cout << "Affichage : \n";
	ab.AffichageParNiveau();	
	cout << endl << "--------------------------------------------" << endl;
	
	return 0;
}
