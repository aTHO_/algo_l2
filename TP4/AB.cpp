/************************************************************************/
/* Auteur : S. Gueye							*/
/* Controle continue 2							*/
/* Date dernière maj : 05/01/2020					*/
/************************************************************************/

#include <iostream>
#include "AB.h"

using namespace std;

//#define DEBUG

/****************************************/
/* Objectif : Constructeur d'un noeud dont les fils sont NULL
/* Entrées : entier x
/* Complexité : 0(1)
/****************************************/
noeud::noeud(int x)
{
	cle = x;
	fg = fd = pere =  NULL;
}
/****************************************/
/* Objectif : Constructeur d'un noeud de fils donnés
/* Entrées : entier x, fg, fd
/* Complexité : 0(1)
/****************************************/
noeud::noeud(int x, noeud* fg, noeud* fd)
{
	cle = x;
	this->fg = fg;
	if(fg)
		fg->pere = this;

	this->fd = fd;
	if(fd)
		fd->pere = this;

}
/****************************************/
/* Objectif : Destructeur d'un noeud
/****************************************/
noeud::~noeud()
{
	if(fg)
		delete(fg);
	if(fd)
		delete(fd);
}
/****************************************/
/* Objectif : Constructeur d'un AB
/****************************************/
AB::AB(noeud* x)
{
	r = x;
}
/****************************************/
/* Objectif : Destructeur d'un AB
/****************************************/
AB::~AB()
{
	if(r)
		delete(r);
}
/****************************************/
/* Objectif : Accesseur à la racine r
/****************************************/
noeud* AB::root()
{
	return(r);
}
/****************************************/
/* Objectif : Méthode récursive renvoyant la hauteur de l'arbre de racine x
/****************************************/
int AB::Hauteur(noeud* x)
{
    if(x == nullptr){
		return(-1);
	} else {
		int maxRight = Hauteur(x->fd);
		int maxLeft = Hauteur(x->fg);

		if(maxRight > maxLeft){
			return(maxRight + 1);
		} else {
			return(maxLeft + 1);
		}
	}
}

/****************************************************************/
/* Objectif : Méthode récursive affichant tous les noeuds	*/
/* se trouvant à la profondeur "prof" dans l'arbre		*/
/****************************************************************/
/* - L'argument "x" permet de se déplacer récursivement dans l'arbre
/* - L'argument "niv" permet de savoir à quelle profondeur l'on se situe
/* - L'argument "prof" est la profondeur pour laquelle on souhaite afficher les valeurs
/* - Seuls les arguments x et niv changent dans les appels récursifs. "prof" est fixé.
(lire main.cpp pour comprendre comment cette méthode est appelée).
/* - Attention : L'affichage du contenu d'un noeud x doit se faire en écrivant : cout << x->cle << " "
/****************************************************************/
void AB::Niveau(noeud* x, int prof, int niv)
{
    if(niv == prof){
		cout << x->cle << " ";
	} else {
		if(x->fg != nullptr){
			Niveau(x->fg, prof, niv+1); // check Left
		}
		if(x->fd != nullptr){
			Niveau(x->fd, prof, niv+1); // check right
		}
	}
	if(niv > prof){
		return;
	}
}

/****************************************************************/
/* Objectif : Affiche tous les noeuds de l'arbre par niveau	*/
/* Après l'affichage d'un niveau, il faut passer à la ligne pour le niveau suivant.
/* Il y a également un saut de ligne pour le dernier niveau.
/****************************************************************/
void AB::AffichageParNiveau()
{
	#ifdef DEBUG
		cout << "void AB::AffichageParNiveau(){" << endl;
	#endif
	int h = this->Hauteur(this->root());
	for(int i = 0; i <= h; i++){
		#ifdef DEBUG
			cout << "this->Niveau(this->root(), " << i << ", 0)" << endl;
			
		#else
			this->Niveau(this->root(), i, 0);
		#endif
		if(i != h){
			cout << endl;
		}
	}
}