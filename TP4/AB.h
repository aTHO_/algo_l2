/************************************************************************/
/* Auteur : S. Gueye							*/
/* TP : Arbres Binaires							*/
/* Date dernière maj : 05/01/2020					*/
/************************************************************************/

/****************************************/
/* noeud contenant un entier un fils    */
/* gauche et un fils droit		*/
/****************************************/
class noeud{
	public:
	int cle;
	noeud* fg;
	noeud* fd;
	noeud* pere;
	noeud(int x);
	noeud(int x, noeud* fg, noeud* fd);
	~noeud();
	void Affiche(noeud* x);
};

/****************************************/
/* Arbre binaire d'entiers		*/
/****************************************/
class AB{
	noeud* r;

	public :
	AB(noeud* x);
	~AB();
	noeud* root();
	int Hauteur(noeud* x);
	void Niveau(noeud* x, int prof, int niv);
	void AffichageParNiveau();
};
